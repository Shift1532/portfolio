export default {
  title: 'Portfolio Baptiste Bernier',
  build:{
    transpile:['three']
  },
  head:{
    script: [
      {
        type: 'module',
        src: '/home/shift/vite/portfolio/node_modules/three/src/Three.js'
      }
    ]
  },
  nitro: {
    preset: 'vercel-edge',
  },
};
